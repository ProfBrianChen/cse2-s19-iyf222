///Iman Faris, CSE002 HW#2, 02/05/19
//Objectives. This homework has the objective of giving you practice manipulating data stored in variables,
//in running simple calculations, and in printing the numerical output that you generated.


public class Arithmetic {
  //main method required for every Java program 
  public static void main(String[] args){
    //our input data
     //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
   
 double totalCostOfPants;   //total cost of pants
   totalCostOfPants = numPants*pantsPrice*paSalesTax*100;
    totalCostOfPants = ((int)totalCostOfPants)/100.0; //shrinks to 2 decimal places 
  
 double totalCostOfShirts; //total cost of shirts 
       totalCostOfShirts = numShirts*shirtPrice*paSalesTax*100;
    totalCostOfShirts = ((int)totalCostOfShirts)/100.0; //shrinks to 2 decimal places 
    
double totalCostOfBelts; //total cost of pants 
    totalCostOfBelts = numBelts*beltCost*paSalesTax*100;
    totalCostOfBelts = ((int)totalCostOfBelts)/100.0; //shrinks to 2 decimal places 
    
    double totalPantsPriceBeforeTax;  //total price of pants before tax 
    totalPantsPriceBeforeTax = numPants*pantsPrice*100;
    totalPantsPriceBeforeTax = ((int)totalPantsPriceBeforeTax)/100.0; //shrinks to 2 decimal places 
    
    double totalBeltsPriceBeforeTax; //total price of belts before tax 
    totalBeltsPriceBeforeTax = numBelts*beltCost*100;
    totalBeltsPriceBeforeTax = ((int)totalBeltsPriceBeforeTax)/100.0; //shrinks to 2 decimal places 
    
    double totalShirtsPriceBeforeTax; //total price of shirts before tax 
    totalShirtsPriceBeforeTax = numShirts*shirtPrice*100;
    totalShirtsPriceBeforeTax = ((int)totalShirtsPriceBeforeTax)/100.0; //shrinks to 2 decimal places 
    
    double totalCostBeforeTax; //total cost before tax
    totalCostBeforeTax = totalShirtsPriceBeforeTax = totalBeltsPriceBeforeTax + totalPantsPriceBeforeTax*100;
    totalCostBeforeTax = ((int)totalCostBeforeTax)/100.0; //shrinks to 2 decimal places 
   
    
    
  double totalPaid; ///total paid for this transaction 
    totalPaid = totalCostOfPants + totalCostOfShirts + totalCostOfBelts*100;
    totalPaid = ((int)totalPaid)/100.0;
    
   double SalesTaxOfPants; // sales tax for pants
    SalesTaxOfPants = totalCostOfPants * 1.06*100;
    SalesTaxOfPants = ((int)SalesTaxOfPants)/100.0; //shrinks to 2 decimal places 
    
    double SalesTaxOfShirts; //sales tax of shirts 
    SalesTaxOfShirts = totalCostOfShirts * 1.06*100;
    SalesTaxOfShirts = ((int)SalesTaxOfShirts)/100.0; //shrinks to 2 decimal places 
    
    double SalesTaxOfBelts; //sales tax of Belts
    SalesTaxOfBelts = totalCostOfBelts * 1.06*100;
    SalesTaxOfBelts = ((int)SalesTaxOfBelts)/100.0; //shrinks to 2 decimal places 
    
    double totalSalesTax; // total Sales Tax 
     totalSalesTax = SalesTaxOfShirts + SalesTaxOfPants + SalesTaxOfBelts*100;
    totalSalesTax = ((int)totalSalesTax)/100.0; //shrinks to 2 decimal places 
      
    //prints Total cost of each kind of item 
    System.out.println("total Cost of Pants = " +totalCostOfPants);  
     System.out.println("total Cost of Shirts = "+totalCostOfShirts);
     System.out.println("total Cost of Belts = "+totalCostOfBelts);
    
//  prints Sales tax charged buying all of each kind of item 
    System.out.println("Sales Tax Charged on Pants: " + SalesTaxOfPants); 
    System.out.println("Sales Tax Charged on Shirts: " + SalesTaxOfShirts );
    System.out.println("Sales Tax Charged on Belts: " + SalesTaxOfBelts);
    
    double TotalPaidSalesTax; //total paid for this transactions 
    TotalPaidSalesTax = totalPaid + totalSalesTax;
    
   System.out.println("Total Cost Before Tax: " + totalCostBeforeTax); // prints Total cost of purchases, before tax
    
   System.out.println("Total Sales Tax: " + totalSalesTax ); //prints Total sales tax
   System.out.println("Total Paid + Sales Tax : " + TotalPaidSalesTax );  //prints Total paid for this transactions, 
    //including sales tax. 
   
  }  
    
}
  