//CSE002, Iman Faris, HW#3. This homework gives you practice in writing code that 
//enables the user to input data and gives you practice in performing arithmetic operations.

//Program #1. The user wants to convert meters to inches, which are stored in doubles.  
//Ask the user for a measurement in meters, then print out the correct number of inches. 

public class Convert {
  //main method required for every Java program 
  public static void main(String[] args){
    
    double metersDistance = 34.23;
      //the distance in meters
    double metersDistance2 = 15.25;
      //the second distance given in meters
      
     double inchesDistance;
    inchesDistance = metersDistance*39.37*100;
    inchesDistance = ((int)inchesDistance)/100.0;

    //meters converted to inches 
    double inchesDistance2;
    inchesDistance2 = metersDistance2*39.37*100;
    inchesDistance2 = ((int)inchesDistance2)/100.0;

    //meters converted to inches 
    
    System.out.println("Enter the distance in meters: " + metersDistance);
    System.out.println(metersDistance + " meters is " + inchesDistance);
    System.out.println("Enter the distance in meters: " + metersDistance2);
    System.out.println(metersDistance2 + " meters is " + inchesDistance2);
    //prints the conversions 
    
  }
  
  
}