//Iman Faris, CSE002, HW#3, 02/11/19
// Write a program that prompts the user for the dimensions of a box: length, width and height. 
//The program should print out the volume inside the box.
import java.util.Scanner;

public class BoxVolume{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
    
    Scanner myScanner = new Scanner( System.in );
    //inputs are ready to be accepted 
    
    System.out.print("Width = ");
    double Width = myScanner.nextDouble();
    //accpets user input 
          
    System.out.print("Length = ");
    double Length = myScanner.nextDouble();
    //accpets user input 
    System.out.print("Height = ");
    double Height = myScanner.nextDouble();
    //accpets user input 
    
   double Volume;
   Volume = Width*Length*Height;
   //calculates the volume of a box 
            
  System.out.println ("Volume = " + Volume);
  //prints the volume of the box  
 
  }
}