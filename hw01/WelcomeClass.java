//CSE 001 HW01

public class WelcomeClass{
public static void main(String[] args){
  ///prints Welcome with Lehigh ID 
  System.out.println("------------");
  System.out.println("| Welcome |");
  System.out.println("------------");
  System.out.println("^  ^  ^  ^  ^  ^");
  System.out.println("//\\//\\//\\//\\//\\//\\");
  System.out.println("<-i--y--f--2--2--2>");
  System.out.println("\\//\\//\\//\\//\\//\\//");
  System.out.println(" v  v  v  v  v  v");
  
  System.out.println("I am a current Freshman at Lehigh University.");
  System.out.println("I hope to major in International Relations and Economics with a minor in CS.");
}
}